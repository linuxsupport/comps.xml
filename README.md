# comps.xml

This repository stores comps.xml files to be used in koji repositories that
require the definition of groups.

One example of a repo that requires groups is the CERN repo.